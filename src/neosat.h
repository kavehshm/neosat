/*
 * neosat.h
 *
 *  Created on: Jun 24, 2021
 *      Author: kaveh
 */

#ifndef SRC_SAT_NEOSAT_H_
#define SRC_SAT_NEOSAT_H_

#include <vector>
#include <string>
#include <set>
#include <list>
#include "idmap.h"
#include "minheap.h"

namespace neosat {

#define sl_True 1
#define sl_False 0
#define sl_Undef 2

typedef int64_t vii;

#define V1(cmd) { if (verbose >= 1) { cmd; } }
#define V2(cmd) { if (verbose >= 2) { cmd; } }
#define V3(cmd) { if (verbose >= 3) { cmd; } }

// class for turnery simulations
class tern_t {
public:
	enum tern_val : int8_t {ZERO = 0, ONE = 1, X = 2};
private:
	tern_val val;
public:
	tern_t() : val(X) {}
	tern_t(tern_val val) : val(val) {}
	tern_t(int v) : val((tern_val)v) {}

	tern_t operator^(tern_t p) const {
		if (val == X || p.val == X)
			return tern_t(X);
		return tern_t(val ^ p.val);
	}

	tern_t operator&(tern_t p) const {
		if (val == ZERO || p.val == ZERO)
			return tern_t(ZERO);
		else if (val == ONE && p.val == ONE)
			return tern_t(ONE);
		return tern_t(X);
	}

	tern_t operator|(tern_t p) const {
		if (val == ONE || p.val == ONE)
			return tern_t(ONE);
		else if (val == ZERO && p.val == ZERO)
			return tern_t(ZERO);
		return tern_t(X);
	}

	bool operator==(tern_t t) const {
		return val == t.val;
	}

	bool operator!=(tern_t t) const {
		return val != t.val;
	}

	tern_t operator~() const {
		if (val == X)
			return tern_t(X);
		else
			return tern_t(val ^ 1);
	}

	int toInt() const {
		return (int)val;
	}

	bool toBool() const {
		assert(val != X);
		return (bool)val;
	}

	friend std::ostream& operator<<(std::ostream& out, tern_t t);
};

inline std::ostream& operator<<(std::ostream& out, tern_t t) {
	switch(t.val) {
	case tern_t::X:
		out << "x";
		break;
	case tern_t::ONE:
		out << "1";
		break;
	case tern_t::ZERO:
		out << "0";
		break;
	default:
		abort();
		break;
	}
	return out;
}

struct slit {
	vii x = -1;
	static slit mkLit(vii i, bool sign = false) { slit ret; ret.x = (i << 1) | (vii)sign; return ret; }
	static slit mkLit_wx(vii x) { slit ret; ret.x = x; return ret; }
	vii ind() const { return x >> 1; }
	bool sgn() const { return x & 1; }
	slit operator~() const { slit r; r.x = x; r.x ^= 1; return r; }
	bool operator==(slit b) { return x == b.x; }
	bool operator!=(slit b) { return x != b.x; }
	friend std::ostream& operator<<(std::ostream& out, slit a);
};

inline bool operator>(slit a, slit b) {
	return (a.ind() > b.ind()) || (a.ind() == b.ind() && (int)a.sgn() > (int)b.sgn());
}
inline bool operator<(slit a, slit b) {
	return b > a;
}
inline bool operator>=(slit a, slit b) {
	return !(a < b);
}
inline bool operator<=(slit a, slit b) {
	return !(a > b);
}

inline slit operator^(slit a, bool b) { slit r; r.x = a.x ^ b; return r; }

const constexpr slit lit_Undef = { -2 };  // }- Useful special constants.
const constexpr slit lit_Error = { -1 };  // }

typedef std::vector<slit> slitvec;
typedef std::set<slit> slitset;

class NeoSat {

protected:

	typedef double dmetric; // decision metric
	typedef int64_t cii; // clause index type in clause database
	typedef int64_t cvi; // variable index type in clause

	struct Clause {
		slitvec lits;
		cvi fc; // free count
		bool is_learned = false;
		bool locked = false;
		dmetric activity = 0;
		std::array<slit, 2> watches;

		cii size() { return lits.size(); }
		slit& operator[](cvi i) { return lits.at(i); }
		bool learnt() { return is_learned; }

		std::string to_string();
	};

	typedef std::vector<Clause> ClauseVec;
	typedef std::pair<cii, cvi> ClauseLitRef;
	typedef utl::idmap<cii, 1, cvi> WatchList;
	typedef std::vector<slit> AssignTrail;

	const static cii cr_NoConf = -1;

	// reasons for variable assignment
	const static constexpr cii cr_NotAssigned = -2; // not assigned yet
	const static constexpr cii cr_DecReason = -3; // picked as decision variable
	const static constexpr vii var_Undef = -1;

	AssignTrail trail;
	std::vector<vii> trail_lim;
	vii qhead = 0;
	std::vector<vii> learnt_clauses;
	slit curdecvar;

	vii num_var = 0;
	cii  num_claus = 0;
	vii dec_level = 0;
	vii num_assigned_var = 0;

	const static int verbose = 0;

    struct VarOrderLt {
        NeoSat* own = nullptr;
        bool operator () (vii x, vii y) const { return own->getVarData(x).activity > own->getVarData(y).activity; }
        VarOrderLt() {}
        VarOrderLt(NeoSat& top) : own(&top) {}
    };

    struct reduceDB_lt {
        NeoSat *own = nullptr;
        reduceDB_lt() {}
        reduceDB_lt(NeoSat& top) : own(&top) {}
        bool operator () (cii x, cii y) {
        	auto& clx = own->getClause(x);
        	auto& cly = own->getClause(y);
            return clx.size() > 2 && (cly.size() == 2 || clx.activity < cly.activity);
        }
    };

    typedef utl::minheap<vii, VarOrderLt> decVarHeap;
	decVarHeap varque;

	struct VarData {
		bool alive = true;
		bool decision_var = true;
		char seen = false;
		bool seenb = false;
		bool polarity = 0;
		int assign_count = 0;
		vii level = -1;
		cii reason = cr_NotAssigned; // reason for assignment
		tern_t cur_assingment = tern_t::X;
		dmetric activity = 0.0;
		WatchList watches;
	};

	dmetric random_seed;

	static constexpr dmetric var_act_cap = 1e100;
	static constexpr dmetric cla_act_cap = 1e100;
	dmetric var_act_inc = 1;
	dmetric cla_act_inc = 1;
    dmetric    var_decay;
    dmetric    clause_decay;

	slitvec assumptions; // assumption vector

	enum {ANALYZE_FIRSTCUT, ANALYZE_1UIP} analyze_method = ANALYZE_1UIP;

	utl::idmap<VarData, 1, vii> vardata_map;
	utl::idmap<Clause, 1, cii> clause_map;
	utl::idmap<bool, 1, vii> model;

	int64_t num_propagations = 0;
	int64_t num_conflicts = 0;

	double    learntsize_inc;
	double    learntsize_factor;
    double max_learnts;
    double learntsize_adjust_confl;
    int32_t learntsize_adjust_cnt;
    double    learntsize_adjust_inc;
    int       learntsize_adjust_start_confl;

	int64_t propagation_limit = -1;
	int64_t conflict_limit = -1;
	int ccmin_mode = 1;

public:
	NeoSat();

	void clear(int new_use_simp = -1);
	slit create_new_var();

	void add_clause(slit l1);
	void add_clause(slit l1, slit l2);
	void add_clause(slit l1, slit l2, slit l3);
	void add_clause(const slitvec& clause);
//	inline slitvec get_unsat_assumps();
//	inline slitset get_unsat_assumps_set();
//	int solve();
//	int solve(slit assump);
//	int solve(slit assump1, slit assump2);
//	void simplify();
//	void set_simp(bool use_simp);
//	void setFrozen(slit p, bool val = true);
//	void setDecVar(slit p, bool val = true);
//	int solve(const std::vector<slit>& assumps);
//	int solve(const std::vector<slit>& assumps, slit assump1);
//	int solve_limited(const std::vector<slit>& assumps, int64_t propBudget);
//	int solve_limited_prog(const std::vector<slit> assumps, int64_t startpropBudget,
//			int64_t maxpropBudget, float growth, bool verbose = false);
//	inline slit true_lit();
//	inline slit false_lit();
//	bool get_value(slit) const;
//	void reseed_solver();
//	void write_dimacs(std::string filename, const std::vector<slit>& assumps);
//	void print_stats();
	vii num_vars() { return vardata_map.size(); }
	cii  num_clauses() { return clause_map.size(); }

	int solve() { return solve_(); }
	int solve(const slitvec& assumps) { assumptions = assumps; return solve_(); }

	std::string to_string();
	void read_dimacs(std::istream& ss);

	static vii var(slit p) { return p.ind(); }

protected:

	enum status_t {STATUS_FRESH, STATUS_UNSAT, STATUS_SAT, STATUS_INTER} status = STATUS_FRESH;

	VarData& getVarData(vii v);
	Clause& getClause(cii cx);
	WatchList& varWatches(slit lt);
	cii attach_clause_(const slitvec& clause);
	bool within_budget();
	void simplify_clause0(slitvec& cl);
	bool litRedundant(slit p);
	void check_all_clauses();
	void sort(slitvec& cl);
	void assignVar(slit a, cii reas = cr_DecReason);
	void unassignVar(slit a);
	cii propagate();
	void pickvar();
	bool decision(vii v);
	bool dequeAssignment(slit& assgn);
	bool getlastAssignment(slit& assgn);
	bool satisfied(Clause& cl);
	void enquepAssign(slit p, cvi reason = cr_DecReason);
	void remove_clause(cii cx);
	vii decisionLevel();
	vii newDecisionLevel();
	void analyze(cii ret, slitvec& conf_cl, vii& bt_level);
	void backtrackTo(vii bt_level);
	vii numAssigned();
	int solve_();
	int search();
	int simplify();
	void reduce_learnt();
	tern_t value(slit lt);
	tern_t value(vii var);
	cii reason(slit lt);
	cii reason(vii v);
	cii level(slit lt);
	cii level(vii v);
	bool seen(vii var);
	void setseen(vii var, bool sval);
	bool litseen(slit var);
	void setlitseen(slit p, bool sval);

	void print_state();
	void print_trail();
	void verify_model();

	inline void varDecayActivity() { var_act_inc *= (1 / var_decay); }
	inline void claDecayActivity() { cla_act_inc *= (1 / clause_decay); }

	dmetric varBumpActivity(vii v) {
		auto& vd = getVarData(v);
		if ((vd.activity += var_act_inc) > var_act_cap) {
			for (auto& p : vardata_map) {
				p.second.activity /= var_act_cap;
			}
			rebuildVarOrder();
		}
		else {
			varque.decrease(v);
		}

		return vd.activity;
	}

	void rebuildVarOrder() {
		varque.clear();
		std::vector<vii> vars;
		for (auto& p : vardata_map) {
			vars.push_back(p.first);
		}
		varque.build(vars);
	}

	dmetric claBumpActivity(cii cx) {
		Clause& cl = getClause(cx);
		if ((cl.activity += cla_act_inc) > cla_act_cap) {
			for (cii cid : learnt_clauses) {
				getClause(cid).activity /= cla_act_cap;
			}
		}
		return cl.activity;
	}

    // Returns a random float 0 <= x < 1. Seed must never be 0.
    static inline double drand(double& seed) {
        seed *= 1389796;
        int q = (int)(seed / 2147483647);
        seed -= (double)q * 2147483647;
        return seed / 2147483647;
    }

    // Returns a random integer 0 <= x < size. Seed must never be 0.
    static inline int irand(double& seed, int size) {
        return (int)(drand(seed) * size);
    }


};


} // namespace neosat

#endif /* SRC_SAT_NEOSAT_H_ */
