
** A Minisat-like C++ STL-based SAT-solver **

This is a C++ SAT solver based on Minisat that uses std::vector instead of Vec<> from Minisat. 

You can build with the makefile. `bash test.sh` should run it on a few random SATLIB benchmarks included in the `./bench` directory.
