/*
 * neosat.cpp
 *
 *  Created on: Jun 24, 2021
 *      Author: kaveh
 */

#include "neosat.h"
#include "utility.h"
#include <iomanip>
#include <algorithm>
#include <queue>

namespace neosat {


NeoSat::NeoSat() :
		learntsize_factor((double)1/(double)3),
		learntsize_inc(1.1)

		// Parameters (experimental):
		//
	   ,learntsize_adjust_start_confl (100)
	   ,learntsize_adjust_inc         (1.5)
{
	varque = decVarHeap(VarOrderLt(*this));
	random_seed = 1;

	var_decay = 0.95;
	clause_decay = 0.999;
	ccmin_mode = 1;
	random_seed = 91648253;

}


slit NeoSat::create_new_var() {

	vii v = vardata_map.create_new_entry();
	return slit::mkLit(v, false);

}

void NeoSat::add_clause(slit l1) {
	slitvec cl = {l1};
	add_clause(cl);
}

void NeoSat::add_clause(slit l1, slit l2) {
	add_clause({l1, l2});
}

void NeoSat::add_clause(slit l1, slit l2, slit l3) {
	add_clause({l1, l2, l3});
}

tern_t NeoSat::value(slit lt) {
	return getVarData(lt.ind()).cur_assingment ^ lt.sgn();
}

tern_t NeoSat::value(vii v) {
	return getVarData(v).cur_assingment;
}

NeoSat::cii NeoSat::reason(vii v) {
	return getVarData(v).reason;
}

NeoSat::cii NeoSat::reason(slit lt) {
	return getVarData(lt.ind()).reason;
}

NeoSat::cii NeoSat::level(vii v) {
	return getVarData(v).level;
}

NeoSat::cii NeoSat::level(slit lt) {
	return getVarData(lt.ind()).level;
}

bool NeoSat::seen(vii var) {
	return getVarData(var).seen;
}

void NeoSat::setseen(vii var, bool svar) {
	getVarData(var).seen = svar;
}


std::string NeoSat::to_string() {
	std::string ret;
	for (auto& p : clause_map) {
		ret += "( ";
		for (auto x : p.second.lits) {
			ret += (x.sgn() ? "~x":"x") + std::to_string(x.ind()) + " ";
		}
		ret += ")\n";
	}
	return ret;
}

std::ostream& operator<<(std::ostream& out, slit a) {
	out << (std::string(a.sgn() ? "~x":"x") + std::to_string(a.ind()));
	return out;
}

std::string NeoSat::Clause::to_string() {
	return "(" + utl::to_delstr(lits, " + ") + ")";
}

void NeoSat::sort(slitvec& cl) {
	std::sort(cl.begin(), cl.end());
}

void NeoSat::add_clause(const slitvec& cl) {

	V1( std::cout << "adding clause (" << utl::to_delstr(cl, " + ") << ")\n");
	if (cl.size() == 0) {
		return;
	}

	slitvec cl1;
	for (cvi i = 0; i < cl.size(); i++) {
		auto curval = value(cl[i]);
		if (curval == tern_t::ONE) {
			return; // clause is true. no need to add
		}
		else if (curval != tern_t::ZERO) {
			cl1.push_back(cl[i]);
		}
	}

	if (cl1.size() == 0) {
		// unsat clause added
		V1( std::cout << "added clause is UNSAT.\n");
		status = STATUS_UNSAT;
		return;
	}

	if (cl1.size() == 1) {
		enquepAssign(cl1[0]);
		if (propagate() == cr_NoConf) {
			V1( std::cout << "unit clause propagation during add_clause concluded.\n");
			if (decisionLevel() == 0) {
				status = STATUS_SAT;
			}
			return;
		}
		else {
			V1( std::cout << "unit propagation during add_clause caused conflict. UNSAT\n");
			status = STATUS_UNSAT;
			return;
		}
	}

	sort(cl1);

	V1(std::cout << "sorted cl1 : (" << utl::to_delstr(cl1, " + ") << ")\n");

	simplify_clause0(cl1);

	V1(std::cout << "post simp clause : (" << utl::to_delstr(cl1, " + ") << ")\n");
	attach_clause_(cl1);

}

// remove redundant lits
void NeoSat::simplify_clause0(slitvec& cl) {

	slitvec scl;
	// remove repeated lits
	slit *i, *j, *e;
	e = &cl[cl.size() - 1];

	for (i = &cl[0]; i <= e;) {
		slit p = *i;
		V1(std::cout << "i is at " << *i << "\n");
		scl.push_back(p);
		for (j = i + 1; j <= e; j++) {
			V1(std::cout << "looking at j:" << *j << "\n");
			if (*j == ~p) {
				V1(std::cout << "complementary literals " << *j << " and " << p << " in clause. Is sat. not adding. skipping");
				scl.clear();
				return;
			}
			else if (*j == p) {
				V1(std::cout << "skipping redundant lit\n");
			}
			else {
				V1(std::cout << "new lit.\n");
				//cl2.push_back(*j);
				break;
			}
		}
		i = j;
	}

	cl = scl;

}


NeoSat::cii NeoSat::attach_clause_(const slitvec& clause) {

	// otherwise add clause to database
	cii clind = clause_map.create_new_entry();
	cvi w0 = -1, w1 = -1;
	for (cvi i = 0; i < clause.size(); i++) {
		if (value(clause[i]) != tern_t::ZERO) {
			getVarData(clause[i].ind()).watches[clind] = i;
			w0 = i;
			break;
		}
	}

	for (cvi i = 0; i < clause.size(); i++) {
		if (i != w0 && value(clause[i]) != tern_t::ZERO) {
			getVarData(clause[i].ind()).watches[clind] =  i;
			w1 = i;
			break;
		}
	}

	assert(w0 != -1);
	bool is_unit = false;
	if (w1 == -1) {
		is_unit = true;
		for (cvi i = 0; i < clause.size(); i++) {
			if (i != w0) {
				getVarData(clause[i].ind()).watches[clind] = i;
				w1 = i;
				break;
			}
		}
	}

	auto& cl = clause_map.at(clind);
	cl.watches = {clause[w0], clause[w1]};
	cl.lits = clause;
	V1( std::cout << "(" << utl::to_delstr(clause, " + ") << ")  wc:" << cl[w0] << ", " << cl[w1] << "     clcount: " << clause_map.size() << "\n");
	cl.fc = clause.size();

	if (is_unit) {
		V1(std::cout << "clause is unit under current assignments. queing it.\n");
		enquepAssign(cl[w0], clind);
	}

	return clind;
}



NeoSat::VarData& NeoSat::getVarData(vii v) {

	try {
		return vardata_map.atff(v);
	}
	catch (std::out_of_range&) {
		throw std::out_of_range("variable index "+ std::to_string(v) + " does not exist");
	}

}

NeoSat::Clause& NeoSat::getClause(cii cx) {

	try {
		return clause_map.atff(cx);
	}
	catch (std::out_of_range&) {
		throw std::out_of_range("clause index "+ std::to_string(cx) + " does not exist");
	}

}


NeoSat::WatchList& NeoSat::varWatches(slit lt) {
	return getVarData(lt.ind()).watches;
}

void NeoSat::assignVar(slit a, cii reas) {
	V1( std::cout << "assigning x" << a.ind() << "=" << !a.sgn() << std::endl);
	auto& vd = getVarData(a.ind());
	auto& ca = vd.cur_assingment;
	if (ca == tern_t::X) {
		num_assigned_var++;
	}
	vd.reason = reas;
	ca = !a.sgn();
	//print_state();
}

void NeoSat::unassignVar(slit a) {
	V1( std::cout << "unassiging x" << a.ind() << std::endl);
	auto& vd = getVarData(a.ind());
	auto& ca = vd.cur_assingment;
	if (ca != tern_t::X) {
		num_assigned_var--;
	}
	ca = tern_t::X;
	vd.reason = cr_NotAssigned;
	vd.seen = false;
	vd.seenb = false;
	vd.level = -1;
	//print_state();
}

bool NeoSat::dequeAssignment(slit& p) {
	if (trail.empty()) {
		return false;
	}
	p = trail.back();
	trail.pop_back();
	return true;
}

bool NeoSat::getlastAssignment(slit& p) {
	if (trail.empty()) {
		return false;
	}
	p = trail.back();
	return true;
}

void NeoSat::check_all_clauses() {
	for (auto& p : clause_map) {
		slit w0 = p.second.watches[0];
		slit w1 = p.second.watches[1];

		if (value(p.second.watches[0]) == tern_t::ZERO
				&& value(p.second.watches[1]) == tern_t::ZERO) {
			std::cout << "clause " << p.first << " : " << p.second.to_string()
					<< " has zero watches " << p.second.watches[0] << " " << p.second.watches[1] << "\n";

			auto& vd0 = getVarData(w0.ind());
			auto& vd1 = getVarData(w1.ind());
			std::cout << "watches for " << w0 << ":\n";
			for (auto& ws : vd0.watches) {
				std::cout << " clause:" << ws.first << "\n";
			}
			std::cout << "watches for " << w1 << ":\n";
			for (auto& ws : vd1.watches) {
				std::cout << " clause:" << ws.first << "\n";
			}
		}
	}
}

// enque and assign
void NeoSat::enquepAssign(slit p, cvi reason) {
	getVarData(p.ind()).level = decisionLevel();
	trail.push_back(p); // variable must be 1 if literal is negative
	assignVar(p, reason);
}

NeoSat::cii NeoSat::propagate() {

	num_propagations++;

	int prop = 0;
	while (true) {

/*
		if (prop++ > 5) {
			exit(1);
		}
*/

		if (qhead >= trail.size()) {
			V1( std::cout << "all facts at lvl " << decisionLevel() << " propagated \n" );
			V1(check_all_clauses());
			return cr_NoConf;
		}

		slit agn = trail[qhead];
		V1(std::cout << "propagating " << agn << " at qhead: " << qhead << "\n");
		qhead++;

		auto& wcl = varWatches(agn);
		for (auto it = wcl.begin(); it != wcl.end(); ) {

			cii clx = it->first;
			auto& cl = getClause(clx);
			V1( std::cout << "checking clause " << clx << " : " << cl.to_string() << std::endl);

			bool zind = (cl.watches[1].ind() == agn.ind());
			slit l0 = cl.watches[zind]; // the currently changing lit
			slit l1 = cl.watches[zind ^ 1]; // unaffected lit
			V1( std::cout << "watches: " << l0 << " " << l1 << std::endl);

			if (value(l0) != tern_t::ZERO) {
				V1( std::cout << "clause is sat. skipping\n");
				// clause is true. skip it.
				++it;
				continue;
			}

			V1( std::cout << "first watch at " << l0 << " is zero. looking for new position.\n");

			bool found_new = false;
			for (cvi i = 0; i < cl.size(); i++) {
				if (cl[i] != l0 && cl[i] != l1 && value(cl[i]) != tern_t::ZERO) {
					// found new lit to watch
					slit nl0 = cl[i];
					it = wcl.erase(it->first);
					V1( std::cout << "moving watch to " << i << " " << nl0 << std::endl);
					getVarData(nl0.ind()).watches[clx] = i;
					cl.watches[zind] = cl[i];
					found_new = true;
					break;
				}
			}
			if (found_new) {
				continue;
			}

			// no new watch found, clause is either unit.
			V1( std::cout << "no new watch found. unit clause \n");
			V1(std::cout << "unit clause producing fact " << l1 << "=1" << " vs " << l1 << "=" << value(l1) << std::endl);
			// check conflict
			if (value(l1) == tern_t::X) {
				enquepAssign(l1, clx);
			}
			else if (value(l1) == tern_t::ZERO) {
				V1(std::cout << "conflict\n");
				return clx;
			}
			else {
				V1(std::cout << "unit clause produces existing fact\n");
			}
			it++;
		}
	}

}

vii NeoSat::decisionLevel() {
	return trail_lim.size();
}

vii NeoSat::newDecisionLevel() {
	trail_lim.push_back(trail.size());
	return trail_lim.size();
}

vii NeoSat::numAssigned() {
	return num_assigned_var;
}

void NeoSat::print_state() {
	for (auto& p : vardata_map) {
		std::cout << "x" << p.first;
		slit lt = slit::mkLit(p.first);
		if (value(lt) != tern_t::X) {
			std::cout << "=" << value(lt);
		}
		std::cout << "  ";
	}
	std::cout << std::endl;
}

void NeoSat::print_trail() {

	std::vector<vii> pos(trail.size(), -1);

	std::cout << "trail_lim:";
	for (vii i = 0; i < trail_lim.size(); i++) {
		std::cout << trail_lim[i] << "  ";
		pos[trail_lim[i]] = i+1;
	}
	std::cout << "\n";

	std::cout << "trail:  ";
	for (vii i = 0; i < trail.size(); i++) {
		std::cout << std::left << std::setw(8) << trail[i];
	}
	std::cout << "\nlims :  ";
	for (vii i = 0; i < trail.size(); i++) {
		std::cout << std::left << std::setw(8) << ( (i == qhead ? "*":"") + (pos[i] == -1 ? "-":std::to_string(pos[i])) );
	}
	std::cout << std::endl;

}

bool NeoSat::litseen(slit p) {
	const auto& vd = getVarData(p.ind());
	return p.sgn() ? vd.seenb : vd.seen;
}

void NeoSat::setlitseen(slit p, bool sval) {
	auto& vd = getVarData(p.ind());
	if (p.sgn()) {
		vd.seenb = sval;
	}
	else {
		vd.seen = sval;
	}
}

// Check if 'p' can be removed from a conflict clause.
bool NeoSat::litRedundant(slit p) {
/*
    enum { seen_undef = 0, seen_source = 1, seen_removable = 2, seen_failed = 3 };
    assert(seen[var(p)] == seen_undef || seen[var(p)] == seen_source);
    assert(reason(var(p)) != CRef_Undef);

    Clause*               c     = &ca[reason(var(p))];
    vec<ShrinkStackElem>& stack = analyze_stack;
    stack.clear();

    for (uint32_t i = 1; ; i++){
        if (i < (uint32_t)c->size()){
            // Checking 'p'-parents 'l':
            Lit l = (*c)[i];

            // Variable at level 0 or previously removable:
            if (level(var(l)) == 0 || seen[var(l)] == seen_source || seen[var(l)] == seen_removable){
                continue; }

            // Check variable can not be removed for some local reason:
            if (reason(var(l)) == CRef_Undef || seen[var(l)] == seen_failed){
                stack.push(ShrinkStackElem(0, p));
                for (int i = 0; i < stack.size(); i++)
                    if (seen[var(stack[i].l)] == seen_undef){
                        seen[var(stack[i].l)] = seen_failed;
                        analyze_toclear.push(stack[i].l);
                    }

                return false;
            }

            // Recursively check 'l':
            stack.push(ShrinkStackElem(i, p));
            i  = 0;
            p  = l;
            c  = &ca[reason(var(p))];
        }else{
            // Finished with current element 'p' and reason 'c':
            if (seen[var(p)] == seen_undef){
                seen[var(p)] = seen_removable;
                analyze_toclear.push(p);
            }

            // Terminate with success if stack is empty:
            if (stack.size() == 0) break;

            // Continue with top element on stack:
            i  = stack.last().i;
            p  = stack.last().l;
            c  = &ca[reason(var(p))];

            stack.pop();
        }
    }*/

    return true;
}

void NeoSat::analyze(cii ret, slitvec& conf_cl, vii& bt_level) {

	auto& cl = clause_map.at(ret);
	V1( std::cout << "analyzing conflict at " << ret << ":" << cl.to_string() << std::endl);
	V1( print_trail() );

	conf_cl.clear();
	bt_level = -1;

	if (analyze_method == ANALYZE_FIRSTCUT) {

		for (cii i = 0; i < cl.size(); i++) {
			cii rc = reason(cl[i]);
			V1( std::cout << rc << "->" << cl[i] << "=" << value(cl[i]) << std::endl);
			if (rc == cr_DecReason) { // decision variable rason
				V1( std::cout << "  is a decision var. ban its state\n");
				assert(value(cl[i]) != tern_t::X);
				conf_cl.push_back( cl[i] ^ value(cl[i]).toBool() );
			}
			else {
				auto& rcl = getClause(rc);
				V1(std::cout << "  going to reason clause. " << rcl.to_string() << ": ");
				for (cvi j = 0; j < rcl.size(); j++) {
					if (rcl[j].ind() != cl[i].ind()) {
						V1( std::cout << "  " << (rcl[j] ^ value(rcl[j]).toBool()) << " ");
						conf_cl.push_back(rcl[j] ^ value(rcl[j]).toBool());
					}
				}
				V1(std::cout << "\n");
			}
		}

		bt_level = decisionLevel() - 1;
		slit decvar = trail.at(trail_lim.at(bt_level));
		getVarData(decvar.ind()).polarity ^= 1;

	}
	else if (analyze_method == ANALYZE_1UIP) {

		std::vector<vii> seenvars;

		vii pathC = 0;
		conf_cl.resize(1);
		vii index = trail.size() - 1;

		slit p;

		do {

			assert(ret != cr_DecReason);

			auto& cl = getClause(ret);

			for (cii i = 0; i < cl.size(); i++) {

				V1(std::cout << cl[i] << "\n");

				auto& vd = getVarData(cl[i].ind());

				if (!vd.seen && vd.level > 0) {
					vd.seen = 1;
					seenvars.push_back(cl[i].ind());
					varBumpActivity(var(cl[i]));
					if (vd.level >= decisionLevel()) {
						V1(std::cout << " is dec\n");
						pathC++;
					}
					else {
						conf_cl.push_back(cl[i]);
						V1(std::cout << " is outdec\n");
					}
				}

			}

	        // Select next clause to look at:
	        while ( !seen(trail.at(index--).ind()) );

			V1(std::cout << " index is " << index << "\n");
			V1(std::cout << " at " << trail.at(index+1) << "\n");
	        p     = trail.at(index+1);

	        ret = reason(p);
	        if (ret == cr_DecReason)
	        	break;
	        V1(std::cout << "looking at clause " << getClause(ret).to_string() << "\n");
	        setseen(p.ind(), 0);
	        pathC--;

		} while (pathC > 0);

		conf_cl[0] = ~p;

	    V1( std::cout << "conflict clause is " << utl::to_delstr(conf_cl, " + ") << "\n");

	    {
			if (ccmin_mode == 2) {
				cvi i, j;
				for (i = j = 1; i < conf_cl.size(); i++)
					if (reason(conf_cl[i]) == cr_DecReason || !litRedundant(conf_cl[i]))
						conf_cl[j++] = conf_cl[i];
				conf_cl.resize(j);

			} else if (ccmin_mode == 1) {
				cvi i, j;
				for (i = j = 1; i < conf_cl.size(); i++){
					vii x = var(conf_cl[i]);

					if (reason(x) == cr_DecReason) {
						conf_cl[j++] = conf_cl[i];
					}
					else {
						Clause& c = getClause( reason(var(conf_cl[i])) );
						for (int k = 1; k < c.size(); k++)
							if (!seen(var(c[k])) && level(var(c[k])) > 0) {
								conf_cl[j++] = conf_cl[i];
								break;
							}
					}
				}
				conf_cl.resize(j);
			}

	    }


		for (auto v : seenvars) {
			setseen(v, 0);
		}

		if (conf_cl.size() == 1) {
		        bt_level = 0;
		}
		else {
			int max_i = 1;
			// Find the first literal assigned at the next-highest level:
			for (cvi i = 2; i < conf_cl.size(); i++)
				if (level(conf_cl[i]) > level(conf_cl[max_i]))
					max_i = i;
			// Swap-in this literal at index 1:
			bt_level = level(conf_cl[max_i]);
		}

	}

	// simplify conflict clause
	sort(conf_cl);
	simplify_clause0(conf_cl);

	V1( std::cout << "final conflict clause is (" << utl::to_delstr(conf_cl, " + ") << ") \n");

}

void NeoSat::backtrackTo(vii bt_level) {

	if (decisionLevel() <= bt_level)
		return;

	assert(bt_level >= 0);

	V1( std::cout << "backtracking to level " << bt_level << std::endl);
	V1( std::cout << utl::to_delstr(trail_lim, " ") << std::endl);
	V1( std::cout << utl::to_delstr(trail, " ") << std::endl);

	V1(print_trail());

	vii ts = trail.size();

	vii tx = trail_lim.at(bt_level);

	for (vii i = ts - 1; i >= tx; i--) {
		unassignVar(trail[i]);
	}

	qhead = tx;
	trail.resize(tx);
	trail_lim.resize(bt_level);
	V1(std::cout << "pbt decision level is " << decisionLevel() << "\n");
	V1( std::cout << "post backtrack\n");
	V1(print_trail());
	//print_state();

}

int NeoSat::solve_() {

	if (status == STATUS_UNSAT) {
		V1( std::cout << "solver in UNSAT state\n");
		return 0;
	}

	search();

	if (status == STATUS_SAT) {
		std::cout << "SATISFIABLE\n";
		V1( std::cout << "model:\n");
		for (auto& p : vardata_map) {
			model[p.first] = value(p.first).toBool();
			V1( std::cout << p.first << ":" << value(p.first) << std::endl);
		}
		V1( std::cout << std::endl);

		verify_model();
		return 1;
	}
	else if (status == STATUS_UNSAT) {
		std::cout << "UNSATISFIABLE\n";
	}

	return 0;

}

void NeoSat::verify_model() {
	bool model_correct = true;
	for (auto& p : clause_map) {
		bool clause_sat = false;
		for (auto l : p.second.lits) {
			if (model.at(l.ind()) ^ l.sgn()) {
				clause_sat = true;
				break;
			}
		}
		if (!clause_sat) {
			std::cout << "clause " << p.first << " -> " << p.second.to_string() << " is not satisfied." << std::endl;
			model_correct = false;
		}
	}

	if (model_correct)
		std::cout << "model is correct" << std::endl;
	else
		std::cout << "model incorrect\n";
}

bool NeoSat::within_budget() {
	if (propagation_limit != -1 && num_propagations >= propagation_limit) {
		return false;
	}
	if (conflict_limit != -1 && num_conflicts >= conflict_limit) {
		return false;
	}
	return true;
}

bool NeoSat::satisfied(Clause& cl) {
	for (auto p : cl.lits) {
		if (value(p) == tern_t::ONE) {
			return true;
		}
	}
	return false;
}


void NeoSat::reduce_learnt()
{
    int     i, j;
    double  extra_lim = cla_act_inc / learnt_clauses.size();    // Remove any clause below this activity

    std::sort(learnt_clauses.begin(), learnt_clauses.end(), reduceDB_lt(*this));
    // Don't delete binary or locked clauses. From the rest, delete clauses from the first half
    // and clauses with activity smaller than 'extra_lim':
    for (i = j = 0; i < learnt_clauses.size(); i++){
        Clause& c = getClause(learnt_clauses[i]);
        if (c.size() > 2 && !c.locked && (i < learnt_clauses.size() / 2 || c.activity < extra_lim))
            remove_clause(learnt_clauses[i]);
        else
            learnt_clauses[j++] = learnt_clauses[i];
    }
    learnt_clauses.resize(j);

}


int NeoSat::simplify() {

	assert(decisionLevel() == 0 && status != STATUS_INTER);

	for (auto& p : clause_map) {
		if (satisfied(p.second)) {
			remove_clause(p.first);
		}
	}

	if (clause_map.size() == 0) {
		V1(std::cout << "all clauses removed, i.e sat in simplify()");
		return 1;
	}

	return 0;
}

void NeoSat::remove_clause(cii cx) {
	assert(decisionLevel() == 0);
	auto& cl = getClause(cx);
	getVarData(cl.watches[0].ind()).watches.erase(cx);
	getVarData(cl.watches[1].ind()).watches.erase(cx);
	clause_map.erase(cx);
}

int NeoSat::search() {

	propagation_limit = -1;
	conflict_limit = -1;

	while (true) {

		assert(decisionLevel() <= num_vars());

		if (!within_budget()) {
			V1(std::cout << "reached conflict limit");
			status = STATUS_INTER;
			break;
		}

		if (status == STATUS_SAT || status == STATUS_UNSAT) {
			break;
		}

		V1( std::cout << "decision level is " << decisionLevel() << std::endl);
		V1(print_trail());

		cii ret = propagate();
		V1( std::cout << "done with propagation. decision level is " << decisionLevel() << std::endl);

		if (ret == cr_NoConf) {
			if (numAssigned() == num_vars()) {
				V1( std::cout << "all variables assigned. SAT.\n");
				status = STATUS_SAT;
				goto done_search;
			}
			else { // have to pick a new variable

	            // Simplify the set of problem clauses:
	            if (decisionLevel() == 0 && simplify()) {
	            	status = STATUS_SAT;
	                goto done_search;
	            }

	            if (learnt_clauses.size() - numAssigned() >= max_learnts) {
	                // Reduce the set of learnt clauses:
	                reduce_learnt();
	            }

				V1( std::cout << "picking new decision var\n");
				pickvar();
				if (curdecvar == lit_Undef) {
					V1(std::cout << "no more decision vars to pick. UNSAT\n");
					status = STATUS_UNSAT;
					goto done_search;
				}
				vii lvl = newDecisionLevel();
				enquepAssign(curdecvar, cr_DecReason);
			}
		}
		else {
			V1( std::cout << "found conflict\n");
			if (decisionLevel() == 0) {
				V1( std::cout << "conflict at level 0. UNSAT.\n");
				status = STATUS_UNSAT;
				goto done_search;
			}

			slitvec conf_cl;
			vii bt_level;
			analyze(ret, conf_cl, bt_level);
			backtrackTo(bt_level);
			if (conf_cl.size() == 1) {
				enquepAssign(conf_cl[0]);
			}
			else {
				cii confid = attach_clause_(conf_cl);
				claBumpActivity(confid);
			}

            varDecayActivity();
            claDecayActivity();

            if (--learntsize_adjust_cnt == 0){
                learntsize_adjust_confl *= learntsize_adjust_inc;
                learntsize_adjust_cnt    = (int)learntsize_adjust_confl;
                max_learnts             *= learntsize_inc;
            }
		}

	}

	done_search:
	V1( std::cout << "done with search\n");

	return 0;
}

bool NeoSat::decision(vii v) {
	return getVarData(v).decision_var;
}

void NeoSat::pickvar() {

	vii next = var_Undef;
	// Activity based decision:
    while (next == var_Undef || value(next) != tern_t::X || !decision(next)) {
        if (varque.empty()) {
            next = var_Undef;
            break;
        }
        else {
            next = varque.removeMin();
        }
    }

    if (next == var_Undef) {
    	curdecvar = lit_Undef;
    }
    else {
    	curdecvar = slit::mkLit(next, getVarData(next).polarity);
    }

}


void NeoSat::read_dimacs(std::istream& iss) {

	std::string line;
	vii line_num = 0;
	vii tokind = 0;
	vii num_vars = 0;
	cii num_clause = 0, read_clauses = 0;
	bool error = false;
	slitvec vars;
	while (getline(iss, line)) {
		line_num++;
		std::vector<std::string> ltokens;
		std::stringstream lss(line);
		std::string token;
		while (getline(lss, token, ' ')) { if (token.size() != 0) ltokens.push_back(token); }

		V1( std::cout << utl::to_delstr(ltokens, ",") << std::endl );
		if (ltokens.size() == 0) {
			continue;
		}

		if (ltokens[0] == "c") {
			continue;
		}

		tokind = 0;
		if (ltokens[tokind] == "p") {
			tokind++;
			if (ltokens[tokind++] != "cnf") { error = true; goto done; }
			num_vars = stoi(ltokens[tokind++]);
			for (vii v = 0; v < num_vars; v++) {
				vars.push_back(create_new_var());
			}
			num_clause = stoi(ltokens[tokind++]);
		}
		else if (ltokens[tokind] == "%") {
			V1( std::cout << "done reading\n" );
			goto done;
		}
		else { // read clause
			slitvec clause;
			for (tokind = 0; tokind < ltokens.size() - 1; tokind++) {
				vii vind = stoi(ltokens[tokind]);
				bool mask = 0;
				if (vind <= 0) {
					mask = 1;
					vind = -vind;
				}
				if (vind > num_vars) {
					V1(std::cout << "variable index " << vind << " is out of bound\n");
					error = true;
					goto done;
				}
				clause.push_back(vars[vind-1] ^ mask);
			}
			add_clause(clause);
			if (++read_clauses == num_clause) {
				V1(std::cout << "read enough clauses" << std::endl);
				goto done;
			}
		}

	}

	done:
	if (error) {
		V1(std::cout << "error at line " << line_num << " token number " << tokind << std::endl);
	}

}

} // namespace neosat




