CC = gcc
FLAGS = -O3 -g3 -fopenmp
CC_FLAGS = $(FLAGS)
CXX = g++-9
CXX_FLAGS = -Wfatal-errors -Wno-register $(FLAGS) -std=c++2a #-static-libstdc++ -static-libgcc #-DUSE_GAUSS=OFF 

BISON = bison
FLEX = flex

BUILD_STATICLIB = false

SAT_SOLVER = glucose # options : minisat, glucose

GUROBI_PWRDEC= false
HW_SCADEC = false
PICOSCOPE_SCADEC = false
VISASCOPE_SCADEC = false


# Final binary
BIN = neosat
SLIB = libneosat.a
# Put all auto generated stuff to this build dir.
BUILD_DIR = ./build
BIN_DIR = ./bin
SOURCE_DIR = ./src

# List of all .cpp source files.
CPP_FILES = $(wildcard ./src/*.cpp)

INCLUDES = -I./src

LIBINCLUDES = -L/usr/local/lib/
STATICLIBS += 
ARSTATICLIBS += 
LDFLAGS += 

# All .o files go to build dir.
OBJ += $(CPP_FILES:%.cpp=$(BUILD_DIR)/%.o)
OBJ += $(CC_FILES:%.cc=$(BUILD_DIR)/%.o)

MAINOBJ = $(wildcard ./bin/main.o)

# Gcc/Clang will create these .d files containing dependencies.
DEP = $(OBJ:%.o=%.d)

# print target
print-%  : ; @echo $* = $($*)

# all target
all : neosat

# Default target named after the binary.
neosat : $(BIN_DIR)/$(BIN)

# Actual target of the binary - depends on all .o files.
$(BIN_DIR)/$(BIN) : $(OBJ)
# Create build directories - same structure as sources.
	mkdir -p $(@D)
# Just link all the object files.
#$(CXX) $(CXX_FLAGS) $(LIBINCLUDES) $(LDFLAGS) $(INCLUDES) $(STATICLIBS) $^ -o $@
	$(CXX) $(CXX_FLAGS) $(LIBINCLUDES) $(OBJ) $(INCLUDES) $(STATICLIBS) $(LDFLAGS) -o $@   
ifeq ($(BUILD_STATICLIB), true)
	ar -rc $(BIN_DIR)/$(SLIB) $(OBJ)
endif


# Include all .d files
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered
# by calling `-include $(DEP)`.
	  
$(BUILD_DIR)/%.o : %.cpp
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) $(INCLUDES) $(VARS) -MMD -c $< -o $@

$(BUILD_DIR)/%.o : %.cc
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) $(INCLUDES) $(VARS) -MMD -c $< -o $@


$(BUILD_DIR)/%.o : %.c
	mkdir -p $(@D)
# The -MMD flags additionaly creates a .d file with
# the same name as the .o file.
	$(CC) $(CC_FLAGS) $(INCLUDES) $(VARS) -MMD -c $< -o $@

$(TESTBIN) : $(BUILD_DIR)/$(TESTBIN)

$(BUILD_DIR)/$(TESTBIN) : $(TESTOBJ) cudd
# Create build directories - same structure as sources.
	mkdir -p $(@D)
# Just link all the object files.
	$(CXX) $(CXX_FLAGS) $(LIBINCLUDES) $(TESTOBJ) $(INCLUDES) $(STATICLIBS) $(LDFLAGS) -o $@

	
.PHONY : clean
clean : 
# This should remove all generated files.
	-rm $(BUILD_DIR)/$(BIN) $(OBJ) $(DEP)


