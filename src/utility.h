/*

 * utility.h
 *
 *  Created on: Sep 21, 2016
 *      Author: kaveh
*/

#ifndef UTILITY_H_
#define UTILITY_H_

#include <sstream>

namespace utl {

// ranged containter to delimited string

template <typename M>
inline std::string to_delstr(const M& set, const std::string& del = "") {
	std::stringstream retss;
	uint i = 0;
	for (const auto& e : set) {
		if (i++ != set.size() - 1)
			retss << e << del;
		else
			retss << e;
	}
	return retss.str();
}

} // namespace utl

#endif
