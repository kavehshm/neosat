import os
import sys

bdir = sys.argv[1]

for fbench in os.listdir(bdir):
	fpath = bdir + './' + fbench
	corrfstr = ''
	with open(fpath, 'r') as fn:
		fstr = fn.read()
		corrfstr = fstr.replace('%\n0', '')
		print(corrfstr)\

	with open(fpath, 'w') as fn:
		fn.write(corrfstr)
