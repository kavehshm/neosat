/*
 * ordque.hpp
 *
 *  Created on: Jul 2, 2021
 *      Author: kaveh
 */

#ifndef SRC_MINHEAP_H_
#define SRC_MINHEAP_H_

#include <vector>
#include <functional>

namespace utl {

template<class K, class Comp>
class minheap {
    Comp   lt;       // The heap is a minimum-heap with respect to this comparator
    std::vector<K> heap;     // Heap of Keys
    std::vector<K> indices;  // Each Key's position (index) in the Heap

    // Index "traversal" functions
    static inline int left  (int i) { return i*2+1; }
    static inline int right (int i) { return (i+1)*2; }
    static inline int parent(int i) { return (i-1) >> 1; }


    void percolateUp(int i)
    {
        K   x  = heap[i];
        int p  = parent(i);

        while (i != 0 && lt(x, heap[p])){
            heap[i]          = heap[p];
            indices[heap[p]] = i;
            i                = p;
            p                = parent(p);
        }
        heap   [i] = x;
        indices[x] = i;
    }


    void percolateDown(int i)
    {
        K x = heap[i];
        while (left(i) < heap.size()){
            int child = right(i) < heap.size() && lt(heap[right(i)], heap[left(i)]) ? right(i) : left(i);
            if (!lt(heap[child], x)) break;
            heap[i]          = heap[child];
            indices[heap[i]] = i;
            i                = child;
        }
        heap   [i] = x;
        indices[x] = i;
    }


  public:
    minheap() {}
    minheap(const Comp& c) : lt(c) {}

    int  size      ()          const { return heap.size(); }
    bool empty     ()          const { return heap.size() == 0; }
    bool inHeap    (K k)       const { return k < indices.size() && indices[k] >= 0; }
    int  operator[](int index) const { assert(index < heap.size()); return heap[index]; }

    void decrease  (K k) { assert(inHeap(k)); percolateUp  (indices[k]); }
    void increase  (K k) { assert(inHeap(k)); percolateDown(indices[k]); }


    // Safe variant of insert/decrease/increase:
    void update(K k)
    {
        if (!inHeap(k))
            insert(k);
        else {
            percolateUp(indices[k]);
            percolateDown(indices[k]); }
    }


    void insert(K k)
    {
        indices.reserve(k, -1);
        assert(!inHeap(k));

        indices[k] = heap.size();
        heap.push_back(k);
        percolateUp(indices[k]);
    }


    void remove(K k)
    {
        assert(inHeap(k));

        int k_pos  = indices[k];
        indices[k] = -1;

        if (k_pos < heap.size()-1){
            heap[k_pos]          = heap.back();
            indices[heap[k_pos]] = k_pos;
            heap.pop_back();
            percolateDown(k_pos);
        }else
            heap.pop_back();
    }


    K removeMin()
    {
        K x              = heap[0];
        heap[0]          = heap.back();
        indices[heap[0]] = 0;
        indices[x]       = -1;
        heap.pop_back();
        if (heap.size() > 1) percolateDown(0);
        return x;
    }


    // Rebuild the heap from scratch, using the elements in 'ns':
    void build(const std::vector<K>& ns) {
        for (int i = 0; i < heap.size(); i++)
            indices[heap[i]] = -1;
        heap.clear();

        for (int i = 0; i < ns.size(); i++) {
            // TODO: this should probably call reserve instead of relying on it being reserved already.
            assert(ns[i] < indices.size());
            indices[ns[i]] = i;
            heap.push_back(ns[i]);
        }

        for (int i = heap.size() / 2 - 1; i >= 0; i--)
            percolateDown(i);
    }

    void clear()
    {
        // TODO: shouldn't the 'indices' map also be dispose-cleared?
        for (int i = 0; i < heap.size(); i++)
            indices[heap[i]] = -1;
        heap.clear();
    }
};


} // namespace utl


#endif /* SRC_MINHEAP_H_ */
