/*
 * main.cpp
 *
 *  Created on: Jun 27, 2021
 *      Author: kaveh
 */

#include "utility.h"
#include "neosat.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

int main(int argc, char** argv) {

	std::vector<std::string> argvec(argv, argv + argc);

	std::cout << utl::to_delstr(argvec, ", ") << "\n";

	using namespace neosat;

	NeoSat S;

	std::string dimacs_file = argvec[1];
	std::ifstream inf(dimacs_file, std::ios::in);

	S.read_dimacs(inf);

	//std::cout << S.to_string() << "\n";

	std::cout << S.solve() << "\n";

}


